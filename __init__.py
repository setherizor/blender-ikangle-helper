# Related links

# Based on
# https://blender.stackexchange.com/questions/34727/why-is-blenders-ik-pole-target-system-so-difficult-am-i-missing-something

bl_info = {
    "name": "Blender IK Angle Helper",
    "author": "setherizor",
    "version": (0, 0, 0),
    "blender": (3, 4, 1),
    "location": "Properties > Bone Constraints > Inverse Kinematics",
    "description": "Simplify IK angle calculation",
    "warning": "",
    "wiki_url": "",
    "category": "Rigging",
}

import bpy
import mathutils
import bmesh

# Math reference
# https://blender.stackexchange.com/questions/19754/how-to-set-calculate-pole-angle-of-ik-constraint-so-the-chain-does-not-move/19755#19755


def signed_angle(vector_u, vector_v, normal):
    # Normal specifies orientation
    angle = vector_u.angle(vector_v)
    if vector_u.cross(vector_v).angle(normal) < 1:
        angle = -angle
    return angle


def get_pole_angle(base_bone, ik_bone, pole_location):
    pole_normal = (ik_bone.tail - base_bone.head).cross(pole_location - base_bone.head)
    projected_pole_axis = pole_normal.cross(base_bone.tail - base_bone.head)
    return signed_angle(
        base_bone.x_axis, projected_pole_axis, base_bone.tail - base_bone.head
    )


# Operator stuff
def IKConstraint_panel(self, context):
    _bone = context.pose_bone

    for cons in _bone.constraints:
        if cons.type == "IK":
            try:
                self.layout.operator(
                    "constraint.adjust_pole",
                    icon="OUTLINER_OB_ARMATURE",
                    text="Adjust Pole Angle:    "
                    + _bone.name
                    + ' ("'
                    + cons.name
                    + '")',
                ).constraint = cons.name
            except:
                pass


class adjust_pole(bpy.types.Operator):
    """Adds functionality to the IK constraint panel."""

    bl_idname = "constraint.adjust_pole"
    bl_label = "Adjust IK Pole Angle"
    bl_icon = "OUTLINER_OB_ARMATURE"

    constraint: bpy.props.StringProperty(
        name="IK Name", description="IK constraint name to set", default=""
    )

    @classmethod
    def poll(cls, context):
        # implement poll
        return True

    def execute(self, context):
        # cache state & get in the right mindset
        context_mode = context.mode
        bpy.ops.object.mode_set(mode="POSE")
        ik_bone = bpy.context.selected_pose_bones_from_active_object[0]
        print("SELECTED IK BONE")
        print(ik_bone)

        # get the base bone
        cons = ik_bone.constraints[self.constraint]
        if cons is None:
            return {"CANCELLED"}
        cons_mute = cons.mute
        base_bone = ik_bone
        for i in range(1, cons.chain_count):
            base_bone = base_bone.parent
        if base_bone is None:
            return {"CANCELLED"}
        print("BASE BONE")
        print(base_bone)

        # get the target bone
        pole_bone = cons.pole_target
        if pole_bone is None:
            return {"CANCELLED"}
        if pole_bone.type == "ARMATURE":
            pole_bone = context.object.pose.bones[cons.pole_subtarget]
        if pole_bone is None:
            return {"CANCELLED"}
        print("POLE BONE")
        print(pole_bone)

        if (base_bone is not None) and (pole_bone is not None):
            cons.mute = True  # TODO: originally false
            bpy.ops.object.mode_set(mode="EDIT")
            pole_angle_in_radians = get_pole_angle(
                base_bone, ik_bone, pole_bone.matrix.translation
            )
            pole_angle_in_deg = round(180 * pole_angle_in_radians / 3.141592, 3)
            print("WE GOT A POLE ANGLE")
            print(pole_angle_in_deg)

            # Cleanup (uncache state) and finish
            cons.mute = cons_mute
            bpy.ops.object.mode_set(mode=context_mode)
            cons.pole_angle = pole_angle_in_radians

        return {"FINISHED"}


classes = [adjust_pole]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    # top button
    bpy.types.BONE_PT_constraints.append(IKConstraint_panel)
    # bottom button
    # bpy.types.BONE_PT_bKinematicConstraint = (
    #     IKConstraint_panel + bpy.types.BONE_PT_bKinematicConstraint
    # )


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    unregister()
    register()
