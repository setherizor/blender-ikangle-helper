# Blender IK Angle Helper

[![Latest Release](https://gitlab.com/setherizor/blender-ikangle-helper/-/badges/release.svg)](https://gitlab.com/setherizor/blender-ikangle-helper/-/releases)
[![pipeline status](https://gitlab.com/setherizor/blender-ikangle-helper/badges/main/pipeline.svg)](https://gitlab.com/setherizor/blender-ikangle-helper/-/commits/main)

## Related Software
- [**Blender 3.4.1**](https://www.blender.org/download/)

## Installation

1. Download the lastest [**BIKAH-[version].zip** release](https://gitlab.com/setherizor/blender-ikangle-helper/-/releases/permalink/latest), DO NOT UNZIP
6. In Blender click `Edit -> Preferences -> Addons -> Install` and select the downloaded zip.
7. You may now delete the zip from your downloads.
